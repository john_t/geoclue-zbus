# Geoclue: The Geoinformation Service

From the GeoClue README:

> Geoclue is a D-Bus geoinformation service. The goal of the Geoclue project
> is to make creating location-aware applications as simple as possible.
> 
> Geoclue is Free Software, licensed under GNU GPLv2+.
> 
> Geoclue comprises the following functionalities:
> - WiFi-based geolocation (accuracy: in meters)
> - GPS(A) receivers (accuracy: in centimeters)
> - GPS of other devices on the local network, e.g smartphones (accuracy: 
>   in centimeters)
> - 3G modems (accuracy: in kilometers, unless modem has GPS)
> - GeoIP (accuracy: city-level)
> 
> WiFi-based geolocation makes use of
> [Mozilla Location Service](https://wiki.mozilla.org/CloudServices/Location).
> 
> If geoclue is unable to find you, you can easily fix that by installing 
> and running a
> [simple app](https://wiki.mozilla.org/CloudServices/Location#Contributing) on 
> your phone. For using phone GPS, you'll need to install the latest version of
> [GeoclueShare app](https://github.com/ankitstarski/GeoclueShare/releases)
> on your phone (currently, this is supported only on Android devices).
> 
> Geoclue was also used for (reverse-)geocoding but that functionality has
> been dropped in favour of the
> [geocode-glib library](http://ftp.gnome.org/pub/GNOME/sources/geocode-glib/).

For reverse Geocoding you can also use
[Nominatim-rs](https://crates.io/crates/nominatim-rs)
